# =============================================================================
# Expects a version of the brewtopia image and upgrades the installed
# dependencies as well as the Utopia installation. Finally, it makes
# the setup compatible for use in the GitLab CI/CD for a models repository.
#
# Suggested base image:  blsqr/brewtopia:v2
#
# NOTE If this build starts to take longer and longer, make sure to run the
#      base image build anew.
# =============================================================================

# Work off the (brewtopia) base image
# NOTE For local builds, may need to specify the platform (e.g. on Apple M*)
ARG BASE_IMAGE
FROM ${BASE_IMAGE}
# FROM --platform=linux/amd64 blsqr/brewtopia:v2

LABEL maintainer="Yunus Sevinchan <ysevinch@iup.uni-heidelberg.de>"

# Update and upgrade dependencies ...
# RUN brew update && brew upgrade
# TODO Parametrise!

# --- armadillo workaround ---------------------------------------------------
# ... for the weirdest errors ever, occuring on brew armadillo 10.1.2 ...
# Also: It's useful to have the same version in the pipeline as available on
# Ubuntu 20.04 LTS (and the cluster), such that errors are easier to detect.

ARG ARMA_VERSION="9.900.4"
RUN    brew uninstall armadillo \
    && brew install wget \
    && wget -O arma_src.tar.xz http://sourceforge.net/projects/arma/files/armadillo-${ARMA_VERSION}.tar.xz \
    && tar -xf arma_src.tar.xz \
    && rm arma_src.tar.xz \
    && ls -la \
    && cd armadillo-${ARMA_VERSION} \
    && cmake . \
    && sudo make install
# TODO superlu support etc??



# ----------------------------------------------------------------------------

# Update Utopia itself, reconfigure, and build a dummy model to see that it's
# all working as expected
# To always run the repository update, include a cache-bust that can be set
# from the outside via `--build-arg CACHEBUST=$(date +%s)`.
# Also need delete the utopia-env (to work around a weird permission issue).
WORKDIR /home/linuxbrew/utopia/utopia

ARG UTOPIA_REPO="https://gitlab.com/utopia-project/utopia.git"
ARG UTOPIA_BRANCH="master"
ARG CACHEBUST=0

RUN    git remote set-url origin ${UTOPIA_REPO} \
    && git fetch --all \
    && git checkout ${UTOPIA_BRANCH} \
    && git pull \
    && echo "${CACHEBUST}"

ENV CC=gcc CXX=g++ CXX_FLAGS="-Og"
RUN    cd build \
    && rm -rf CMakeCache.txt utopia-env \
    && cmake -DCMAKE_BUILD_TYPE=Debug \
             -DCMAKE_CXX_FLAGS_DEBUG="-Og" \
             -DHDF5_ROOT=$(brew --prefix hdf5) \
             -DCMAKE_EXPORT_PACKAGE_REGISTRY=On \
             .. \
    && make dummy \
    && ./run-in-utopia-env utopia run dummy
ENV Utopia_DIR=/home/linuxbrew/utopia/utopia/build

# Done. Overwrite entry point to make it compatible with GitLab CI/CD
WORKDIR /home/linuxbrew
ENTRYPOINT [ ]
