# Place config files in binary dir, needed by some tests
file(
    COPY
        "experimental.yml"
    DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
)

# Collect test target names
set(TEST_NAMES
    experimental
)

# Add UTILS-prefixed unit test targets
foreach(test_name ${TEST_NAMES})
    add_unit_test(NAME test_${test_name}
                  GROUP utils
                  SOURCES test_${test_name}.cc)
endforeach(test_name)
