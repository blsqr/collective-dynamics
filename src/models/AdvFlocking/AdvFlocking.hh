#ifndef UTOPIA_MODELS_ADVFLOCKING_HH
#define UTOPIA_MODELS_ADVFLOCKING_HH

#include <random>

#include <utopia/core/model.hh>
#include <utopia/core/types.hh>

#include <UtopiaUtils/utils/action_manager.hh>
#include <UtopiaUtils/data_io/data_writer.hh>

#include "utils.hh"



namespace Utopia::Models::AdvFlocking {

// ++ Type definitions ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// Type helper to define types used by the model
using ModelTypes = Utopia::ModelTypes<ModelRNG>;


// ++ Model definition ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/// The AdvFlocking Model
class AdvFlocking:
    public Model<AdvFlocking, ModelTypes>
{
public:
    /// The type of the Model base class of this derived class
    using Base = Model<AdvFlocking, ModelTypes>;

    /// Export Base model's time type
    using Time = typename Base::Time;

    /// The data writer type to use
    using DataWriter = Utopia::DataIO::DataWriter::DataWriter;

private:
    // -- Members -------------------------------------------------------------

    /// A manager making dynamically performing config-configurable actions
    ActionManager _am;

    /// The data writer instance, takes care of writing output data
    DataWriter _dw;

    /// Names of dynamically mapped datasets
    static inline const std::vector<std::string> _dynamic_mappings = {
        // "foo/bar_ids"
    };

    /// Uniform real distribution for evaluating probabilities
    std::uniform_real_distribution<double> _prob_distr;


public:
    // -- Model Setup ---------------------------------------------------------
    /// Construct the AdvFlocking model
    /** \param name     Name of this model instance
     *  \param parent   The parent model this model instance resides in
     */
    template<class ParentModel>
    AdvFlocking (const std::string& name, ParentModel& parent)
    :
        Base(name, parent)

        // Set up ActionManager and DataWriter
    ,   _am(this->_log, this->_cfg["action_manager"])
    ,   _dw(*this)

        // Statistical distributions
    ,   _prob_distr(0., 1.)

        // Temporary variables
        // ...
    {
        setup_action_manager();

        // ... and let it perform further setup routines
        this->_am.invoke_hook("setup");

        // Can do further setup here
        // ...

        // Now register all write properties
        register_write_properties();

        this->_log->info("{} is all set up now :)", this->_name);
    }


private:
    // .. Setup functions .....................................................

    // .. Helper functions ....................................................

public:
    // -- Simulation Control --------------------------------------------------

    /// Called before model iteration starts
    /** This override merely adds the ``prolog`` hook, then invokes the parent
      * method.
      */
    void prolog () override {
        this->_am.invoke_hook("prolog");
        this->__prolog();
    }

    /// Called after model iteration ends
    /** This override merely adds the ``epilog`` hook, then invokes the parent
      * method.
      */
    void epilog () override {
        this->_am.invoke_hook("epilog");
        this->__epilog();
    }

    /// Iterate a single step
    void perform_step () {
        this->_am.invoke_hook("perform_step");

        // Might need some cleaning up, i.e.: removing inactive entities
        // if (_need_cleanup) {
        //     // Do cleanup operation ehre

        //     // With cleanup done, the ID map is no longer up to date
        //     _dw.mark_mappings_invalidated(_dynamic_mappings);
        // }

        // .. Main algorithm . . . . . . . . . . . . . . . . . . . . . . . . .
        // -- 1: ... --

        // -- 2: ... --

        // -- 3: ... --
    }


    /// Supply monitoring values
    /** Keys provided by this monitor:
      *
      *    - `...`
      *
      * Furthermore, this is hooked to the `monitor` ActionManager hook.
      */
    void monitor () {
        this->_am.invoke_hook("monitor");
    }


    /// Write data
    /** As the number of active populations and flows differs, the data writing
      * has to store the output in differently-sized datasets.
      * This work is carried out by the mesonet::DataWriter, which handles the
      * tracking and creation of ID mappings and the associated properties.
      *
      * Furthermore, this is hooked to the ``write_data`` ActionManager hook.
      */
    void write_data () {
        this->_am.invoke_hook("write_data");

        // Invoke the DataWriter and let it do all the rest
        _dw.write(this->get_time());
    }


    // -- Getters and setters -------------------------------------------------
    // Add getters and setters here to interface with other model


private:
    // -- Manager setup -------------------------------------------------------

    /// Registers all conceivable output properties to the DataWriter
    void register_write_properties () {
        using namespace Utopia::DataIO::DataWriter;

        this->_log->info("Registering properties with DataWriter ...");

        // .. Helper functions ................................................
        // ... generating write functions from entities or graph iterations

        // const auto from_populations =
        //     [this](const auto& adptr) {
        //         return
        //             [&](const auto& dset){
        //                 auto pops = this->_econet.populations();
        //                 dset->write(pops.begin(), pops.end(), adptr);
        //             };
        //     };


        // .. Re-usable adaptor functions . . . . . . . . . . . . . . . . . . .
        // ... for _all_ entities
        // const auto adptr_id =
        //     [](const auto& e){ return e->id; };
        // const auto adptr_kind_id =
        //     [](const auto& e){ return e->kind_id(); };
        // const auto adptr_active =
        //     [](const auto& e){
        //         return static_cast<char>(e->is_active());
        //     };


        // .. Max extent calculation . . . . . . . . . . . . . . . . . . . . .
        // 1D
        [[maybe_unused]] const auto max_extent_1d_write_ops =
            [](const auto num_write_ops) -> std::vector<hsize_t> {
                return {num_write_ops};
            };

        // 2D
        [[maybe_unused]] const auto max_extent_2d_vertices =
            [](const auto num_write_ops) -> std::vector<hsize_t> {
                return {num_write_ops, 666};  // TODO Use info from capture
            };


        // .. Attribute writers . . . . . . . . . . . . . . . . . . . . . . . .


        // ....................................................................
        // .. PROPERTY REGISTRATION ...........................................

        // Register an ID mapping
        // _dw.register_property("populations/regular/ids",
        //     // The mode of this property
        //     PropMode::mapping,
        //     // The write function, here generated by a helper function
        //     from_regular_populations(adptr_id),
        //     // The (required) max extent function, 1D: {size of container}
        //     max_extent_1d_regular
        //     // Optional: lambda to set additional attributes
        //     // Optional: lambda to calculate chunk sizes; not given -> auto
        // );

        // _dw.register_property("populations/regular/active",
        //     PropMode::mapped, "populations/regular/ids",
        //     from_regular_populations(adptr_active),
        //     max_extent_2d_regular
        // );


        // .. PROPERTY REGISTRATION - GLOBAL PROPERTIES .......................

        // _dw.register_property("econet/available_resources",
        //     PropMode::standalone,
        //     [this](const auto& dset){
        //         auto& econet = this->_econet;
        //         dset->write(
        //             econet.available_resources(econet.filtered.only_regular)
        //         );
        //     },
        //     // NOTE Not using structured dtype here but writing as 2D array
        //     max_extent_2d_rspace_dims,
        //     attrs_rspace_dims
        // );

        // _dw.register_property("econet/mean_composition",
        //     PropMode::standalone,
        //     [this](const auto& dset){
        //         auto& econet = this->_econet;
        //         dset->write(
        //             econet.mean_composition(econet.filtered.only_regular)
        //         );
        //     },
        //     // NOTE Not using structured dtype here but writing as 2D array
        //     max_extent_2d_rspace_dims,
        //     attrs_rspace_dims
        // );


        // .. Finished. . . . . . . . . . . . . . . . . . . . . . . . . . . . .
        this->_log->info("Property registration finished.");
    }

    /// Registers triggers and actions with the ActionManager
    void setup_action_manager () {
        using Time = typename Base::Time;

        // .. Triggers ........................................................
        _am.register_trigger("time",
            [this](const auto& cfg){
                return get_as<Time>("time", cfg) == this->get_time();
            }
        );
        _am.register_trigger("times",
            // Very naive implementation, slow if many times are given
            [this](const auto& cfg){
                using Times = std::vector<Time>;

                return contains(get_as<Times>("times", cfg), this->get_time());
            }
        );
        _am.register_trigger("time_multiples",
            [this](const auto& cfg){
                return (this->get_time() % get_as<Time>("n", cfg) == 0);
            }
        );
        _am.register_trigger("time_slice",
            [this](const auto& cfg){
                const auto start = get_as<Time>("start", cfg, 0);
                const auto stop = get_as<Time>("stop", cfg,
                                               this->get_time_max() + 1);
                const auto step = get_as<Time>("step", cfg);
                const auto time = this->get_time();
                return
                    time >= start and (time-start) % step == 0 and time < stop;
            }
        );


        // .. Actions .........................................................
        // _am.register_action("add_populations",
        //     [this](const auto& cfg){
        //         const auto num = get_as<int>("num", cfg);
        //         const auto kind = get_as<std::string>("kind", cfg);
        //         const auto pop_cfg = recursive_update(
        //             get_as<Config>(kind, this->_defaults["populations"]),
        //             get_as<Config>("init", cfg, {})
        //         );
        //         this->_econet.add_populations(num, kind, pop_cfg);
        //     }
        // );


        // .. Procedures and Hooks ............................................
        const auto& setup_cfg = this->_cfg["setup"];
        if (setup_cfg["procedures"]) {
            for (const auto& kv : setup_cfg["procedures"]) {
                _am.register_procedure(kv.first.template as<std::string>(),
                                       kv.second);
            }
        }

        // From a bunch of scenarios, use a specific one for the setup hook.
        // This allows to easily switch between them.
        // If `use_scenario` is not given or Null, still register the hook, but
        // specify an empty procedure sequence.
        auto procedure_seq = ActionManager::ProcedureSequence {};

        if (setup_cfg["use_scenario"] and
            not setup_cfg["use_scenario"].IsNull())
        {
            const auto scenario = get_as<std::string>("use_scenario",
                                                      setup_cfg);
            this->_log->info("Using scenario '{}' for setup.", scenario);

            const auto& scenarios = get_as<Config>("scenarios", setup_cfg);
            procedure_seq =
                get_as<ActionManager::ProcedureSequence>(scenario, scenarios);
        }
        else {
            this->_log->info("No setup scenario specified.");
        }

        _am.register_hook("setup", procedure_seq);
    }

};


} // namespace Utopia::Models::AdvFlocking

#endif // UTOPIA_MODELS_ADVFLOCKING_HH
