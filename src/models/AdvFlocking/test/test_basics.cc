#define BOOST_TEST_MODULE test basics

#include <boost/test/unit_test.hpp>

#include "testtools.hh"



// ++ Definitions and Fixtures ++++++++++++++++++++++++++++++++++++++++++++++++

using namespace Utopia::Models::AdvFlocking;

/// The specialized infrastructure fixture
struct Infrastructure : public BaseInfrastructure<> {
    Infrastructure () : BaseInfrastructure<>("basics.yml") {}
};



// ++ Tests +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

BOOST_FIXTURE_TEST_SUITE (test_basics, Infrastructure)

/// Test that the model runs ...
BOOST_AUTO_TEST_CASE (test_that_it_runs)
{
    // TODO Define a basic test here... or delete and replace
}

BOOST_AUTO_TEST_SUITE_END() // "test_basics"
