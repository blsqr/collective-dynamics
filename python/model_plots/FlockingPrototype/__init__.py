"""Model plots and custom operations for the FlockingPrototype"""

from .operations import *
from .plots import *
