"""Plots that are specific to the AdvFlocking model"""

# Import the module to trigger registration of operations
from .operations import *
