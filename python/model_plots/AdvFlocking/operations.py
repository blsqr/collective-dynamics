"""Additional dantro data transformation operations"""

from typing import Union, Sequence

import numpy as np
import xarray as xr
import dantro as dtr

from utopya.eval import is_operation

# -----------------------------------------------------------------------------
