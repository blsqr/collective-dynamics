# Pass model information to utopya  ...........................................

# Declare the directories containing the models tests and plots
set(UTOPIA_PYTHON_MODEL_TESTS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/model_tests
    CACHE STRING "the directory containing all python-based model tests" FORCE)
set(UTOPIA_PYTHON_MODEL_PLOTS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/model_plots
    CACHE STRING "the directory containing all python-based model plots" FORCE)
# TODO Remove redundancy

# Register the existing models and associated frontend-related information
register_models_with_utopya()



# Register project and models for Python model implementations ................

# The project, suffixed with `-Python`
message(
    STATUS
        "Registering additional project for Python model implementations ..."
)
execute_process(
    COMMAND
        ${RUN_IN_UTOPIA_ENV}
        utopya projects register "${CMAKE_CURRENT_SOURCE_DIR}"
        --custom-name "${CMAKE_PROJECT_NAME}-Python"
        --require-matching-names
        --exists-action update
    RESULT_VARIABLE RETURN_VALUE
    OUTPUT_VARIABLE CLI_OUTPUT
    ERROR_VARIABLE CLI_OUTPUT
)
if (NOT RETURN_VALUE EQUAL "0")
    message(
        SEND_ERROR "Error ${RETURN_VALUE} registering project "
                   "'${CMAKE_PROJECT_NAME}-Python':\n${CLI_OUTPUT}\n"
                   "You can use `utopya projects` to manually fix the "
                   "registry entry."
    )
    return()
endif()
message(
    STATUS "Updated project registry entry for '${CMAKE_PROJECT_NAME}-Python'."
)


# Models, based on their own manifest files
file(
    GLOB PY_MODEL_MANIFEST_FILES
    ${CMAKE_CURRENT_SOURCE_DIR}/models/*/*_info.yml
)
if (NOT "${PY_MODEL_MANIFEST_FILES}" STREQUAL "")
    list(LENGTH "${PY_MODEL_MANIFEST_FILES}" NUM_PY_MODEL_MANIFESTS)
    message(
        STATUS
            "Providing information on ${NUM_PY_MODEL_MANIFESTS} "
            "Python models to utopya ..."
    )
    execute_process(
        COMMAND
            ${RUN_IN_UTOPIA_ENV}
            utopya models register from-manifest "${PY_MODEL_MANIFEST_FILES}"
            --label added_by_cmake
            --set-default
            --project-name "${CMAKE_PROJECT_NAME}-Python"
            --exists-action overwrite
        RESULT_VARIABLE RETURN_VALUE
        OUTPUT_VARIABLE CLI_OUTPUT
        ERROR_VARIABLE CLI_OUTPUT
    )
    if (NOT RETURN_VALUE EQUAL "0")
        message(
            SEND_ERROR "Error ${RETURN_VALUE} in adding or updating models in "
                       "registry:\n${CLI_OUTPUT}\n"
                       "You can use `utopya models` and `utopya projects` "
                       "to manually fix the registry entries."
        )
        return()
    endif()

    message(
        STATUS "Added or updated registry entries for Python models.\n"
    )
else()
    message(STATUS "No Python models available to register.")
endif()






# Installations ...............................................................
message(STATUS "Installing further Python requirements ...\n")

execute_process(
    COMMAND
        ${RUN_IN_UTOPIA_ENV}
        pip install -r ${CMAKE_CURRENT_SOURCE_DIR}/requirements.txt
    RESULT_VARIABLE RETURN_VALUE
    OUTPUT_VARIABLE PIP_OUTPUT
)
if (NOT RETURN_VALUE EQUAL "0")
    message(WARNING "Failed installing additional python requirements! "
                    "Install the packages defined in the "
                    "python/requirements.txt file manually to remove this "
                    "warning. Output of `pip install` was:\n${PIP_OUTPUT}")
endif()


# Tests .......................................................................

# Add target for python model tests, accessible via `make test_models_python`
add_custom_target(
    test_models_python
    COMMAND cd ${CMAKE_CURRENT_SOURCE_DIR} &&
        ${RUN_IN_UTOPIA_ENV} python -m pytest -vv model_tests/
)
# TODO Add targets for inidivdual python model tests (elsewhere)

# For running all python model tests, it is required that all models are built
# add_dependencies(test_models_python all)  # FIXME somehow, all is not defined

# Add the python model tests as a dependency for the test_models target
add_dependencies(test_models test_models_python)


message(STATUS "Python setup complete.\n")
