# New CMake policy stack:
# Use <Package>_ROOT variables in respective find_package functions/modules
cmake_policy(PUSH)
if (POLICY CMP0074)
    cmake_policy(SET CMP0074 NEW)
endif ()


# --- Find dependencies
# Threads (required by spdlog with gcc)
set(CMAKE_THREAD_LIBS_INIT "-lpthread")
set(CMAKE_HAVE_THREADS_LIBRARY 1)
set(CMAKE_USE_WIN32_THREADS_INIT 0)
set(CMAKE_USE_PTHREADS_INIT 1)
set(THREADS_PREFER_PTHREAD_FLAG ON)

find_package(Threads REQUIRED)

# Required dependencies, partly with more recent versions than in Utopia
find_package(Armadillo 9.900 REQUIRED)
find_package(yaml-cpp 0.6.3 REQUIRED)
find_package(spdlog 1.7 REQUIRED)
find_package(fmt 7.0 REQUIRED)


# Utopia -- for sure!
find_package(Utopia REQUIRED)
# NOTE The UtopiaUtils package is also required, but is included via submodule


# Optional dependencies
find_package(Doxygen OPTIONAL_COMPONENTS dot)




# Done. Remove latest policy stack
cmake_policy(POP)
